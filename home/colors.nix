{
  warm = "#FCE283";
  warn = "#FFA500";
  good = "#2AAFBE";
  black = "#07152F";
  dark = "#023459";
}
